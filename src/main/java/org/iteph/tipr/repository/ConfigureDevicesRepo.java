package org.iteph.tipr.repository;

import org.iteph.tipr.model.ConfigureDevicesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigureDevicesRepo extends JpaRepository<ConfigureDevicesEntity, Long>, JpaSpecificationExecutor<ConfigureDevicesEntity> {
}
