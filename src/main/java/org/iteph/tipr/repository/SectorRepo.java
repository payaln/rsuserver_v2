package org.iteph.tipr.repository;

import org.iteph.tipr.model.SectorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SectorRepo extends JpaRepository<SectorEntity, Long>, JpaSpecificationExecutor<SectorEntity> {
}
