package org.iteph.tipr.repository;

import org.iteph.tipr.model.ControlKeyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ControlKeyRepo extends JpaRepository<ControlKeyEntity, Integer> {
    List<ControlKeyEntity> findAllByIdDevice(Integer id);
}
