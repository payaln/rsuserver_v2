package org.iteph.tipr.repository;

import org.iteph.tipr.model.MessageKeyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageKeyRepo extends JpaRepository<MessageKeyEntity, Integer> {
    List<MessageKeyEntity> findAllByIdDevice(Integer id);
}
