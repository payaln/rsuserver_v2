package org.iteph.tipr.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.iteph.tipr.model.ConfigureDevicesEntity;
import org.iteph.tipr.model.SectorEntity;
import org.iteph.tipr.model.UserEntity;
import org.iteph.tipr.repository.ConfigureDevicesRepo;
import org.iteph.tipr.repository.SectorRepo;
import org.iteph.tipr.specification.CriteriaParser;
import org.iteph.tipr.specification.GenericSpecification;
import org.iteph.tipr.specification.GenericSpecificationsBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@Api(value = "device", description = "Устройства")
public class DeviceController {

    @Autowired
    private ConfigureDevicesRepo configureDevicesRepo;

    @Autowired
    private SectorRepo sectorRepo;

    @ApiOperation(value = "Возвращает все устройства", response = ConfigureDevicesRepo.class)
    @RequestMapping(value = "devices", headers = "Accept=application/json", method = RequestMethod.GET)
    @ResponseBody
    public List<ConfigureDevicesEntity> getDevices(){
        return configureDevicesRepo.findAll();
    }

    @ApiOperation(value = "Поиск устройств", response = ConfigureDevicesRepo.class)
    @RequestMapping(value = "devices/spec", headers = "Accept=application/json", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<ConfigureDevicesEntity> findDevices(@PageableDefault(value = Integer.MAX_VALUE) Pageable pageable,
                                                        @RequestParam(value = "search", required = false) String search){
        Specification<ConfigureDevicesEntity> spec = resolveSpecificationFromInfixExpr(search);
        return configureDevicesRepo.findAll(spec, pageable);
    }
    private Specification<ConfigureDevicesEntity> resolveSpecificationFromInfixExpr(String searchParameters) {
        CriteriaParser parser = new CriteriaParser();
        GenericSpecificationsBuilder<ConfigureDevicesEntity> specBuilder = new GenericSpecificationsBuilder<>();
        return specBuilder.build(parser.parse(searchParameters), GenericSpecification::new);
    }

//    @ApiOperation(value = "Добавляет новый сектор в базу")
//    @Transactional
//    @RequestMapping(value = "sector/add", method = RequestMethod.POST)
//    public ResponseEntity<?> addSector(@RequestBody SectorEntity sector) {
//        try {
//            sectorRepo.save(sector);
//            return ResponseEntity.status(HttpStatus.CREATED).build();
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.CONFLICT).build();
//        }
//    }

    @ApiOperation(value = "Добавляет новое устройство в базу")
    @Transactional
    @RequestMapping(value = "device/add", method = RequestMethod.POST)
    public ResponseEntity<?> addDevice(@RequestBody ConfigureDevicesEntity device) {
        try {
            configureDevicesRepo.save(device);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

}
