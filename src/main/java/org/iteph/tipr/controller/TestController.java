package org.iteph.tipr.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Api(value = "test", description = "тестовый контроллер")
public class TestController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @ApiOperation(value = "тест сокетов")
    @RequestMapping(value = "test/sock", headers = "Accept=application/json", method = RequestMethod.POST)
    public ResponseEntity<?> testSock(@RequestBody String msg){
        System.out.println(msg);
        simpMessagingTemplate.convertAndSend("/test", msg);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "запуск скрипта")
    @RequestMapping(value = "listener/run", headers = "Accept=application/json", method = RequestMethod.POST)
    public ResponseEntity<?> runListener(){
        try {
            System.out.println(System.getProperty("user.dir"));
            Process p = Runtime.getRuntime().exec("python s.py");
            return ResponseEntity.status(HttpStatus.OK).build();
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }
}
