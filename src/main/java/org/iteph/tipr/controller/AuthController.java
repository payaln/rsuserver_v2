package org.iteph.tipr.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.iteph.tipr.model.ConfigureDevicesEntity;
import org.iteph.tipr.model.MessageKeyEntity;
import org.iteph.tipr.model.RedisMSG;
import org.iteph.tipr.payload.JwtAuthResponse;
import org.iteph.tipr.payload.LoginRequest;
import org.iteph.tipr.repository.ConfigureDevicesRepo;
import org.iteph.tipr.repository.ControlKeyRepo;
import org.iteph.tipr.repository.MessageKeyRepo;
import org.iteph.tipr.repository.UserRepo;
import org.iteph.tipr.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/auth")
@Api(value = "auth", description = "Авторизация")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepo userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    private static boolean init = false;

    @Value("${app.redisHost}")
    private String host;
    @Value("${app.redisPort}")
    private int port;

    private Jedis jedis;
    private Jedis geter;
    private List<String> keys = new ArrayList<>();

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    ConfigureDevicesRepo configureDevicesRepo;

    @Autowired
    MessageKeyRepo messageKeyRepo;
    @Autowired
    ControlKeyRepo controlKeyRepo;

    @ApiOperation(value = "Возвращает token по логину и паролю")
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);

        if(!init){
            initRedis I = new initRedis();
            I.start();
            init = true;
        }

        return ResponseEntity.ok(new JwtAuthResponse(jwt));
    }

    class initRedis extends Thread{
        public initRedis() {
        }

        @Override
        public void run() {
            jedis = new Jedis(host, port);
            geter = new Jedis(host, port);
            List<ConfigureDevicesEntity> devices = configureDevicesRepo.findAll();
            for(ConfigureDevicesEntity dev : devices){
                List<MessageKeyEntity> mkeys = messageKeyRepo.findAllByIdDevice(dev.getId());
                if(!mkeys.isEmpty())
                for(MessageKeyEntity mkey : mkeys){
                    keys.add(dev.getDev_name() + ":" + mkey.getKey());
                }
            }

            jedis.configSet("notify-keyspace-events", "KEA");
            jedis.psubscribe(new JedisPubSub() {
                @Override
                public void onPMessage(String pattern, String channel, String message) {
                    super.onPMessage(pattern, channel, message);
                    if(keys.contains(message)){
                        String value = geter.get(message);
                        System.out.println(value);
                        RedisMSG txt = new RedisMSG(message, value);
                        simpMessagingTemplate.convertAndSend("/redis", txt);
                    }
                }
            }, "__keyevent@0__:set");
        }
    }

}
