package org.iteph.tipr.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.iteph.tipr.model.RedisMSG;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;


@Controller
@Api(value = "redis", description = "Работа с redis")
public class RedisController {

    @Value("${app.redisHost}")
    private String host;
    @Value("${app.redisPort}")
    private int port;

    @ApiOperation(value = "Поиск по ключу", response = RedisMSG.class)
    @RequestMapping(value = "redis/get/{key}", headers = "Accept=application/json", method = RequestMethod.GET)
    @ResponseBody
    public RedisMSG getKey(@PathVariable("key") String key){
        Jedis client = new Jedis(host, port);
        String value = client.get(key);
        client.close();
        return new RedisMSG(key, value);
    }

    @ApiOperation(value = "Запись ключа")
    @RequestMapping(value = "redis/set/{key}/{value}", method = RequestMethod.POST)
    public ResponseEntity<?> setKey(@PathVariable("key") String key, @PathVariable("value") String value){
        try {
            Jedis client = new Jedis(host, port);
            client.set(key, value);
            client.close();
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

}
