package org.iteph.tipr.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.iteph.tipr.model.UserEntity;
import org.iteph.tipr.payload.UserSummary;
import org.iteph.tipr.repository.UserRepo;
import org.iteph.tipr.security.CurrentUser;
import org.iteph.tipr.security.UserPrincipal;
import org.iteph.tipr.specification.CriteriaParser;
import org.iteph.tipr.specification.GenericSpecification;
import org.iteph.tipr.specification.GenericSpecificationsBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@Api(value = "user", description = "Пользователи")
public class UserController {
    @Autowired
    private UserRepo userRepo;

    @ApiOperation(value = "Возвращает текущего пользователя", response = UserSummary.class)
    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser){
        return new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getName());
    }

    @ApiOperation(value = "Возвращает список пользователей с возможностью поиска", response = Iterable.class)
    @RequestMapping(value = "users/spec", headers = "Accept=application/json", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<UserEntity> getUsersSpec(@PageableDefault(value = Integer.MAX_VALUE) Pageable pageable,
                                             @RequestParam(value = "search", required = false) String search){
        Specification<UserEntity> spec = resolveSpecificationFromInfixExpr(search);
        return this.userRepo.findAll(spec, pageable);
    }
    private Specification<UserEntity> resolveSpecificationFromInfixExpr(String searchParameters) {
        CriteriaParser parser = new CriteriaParser();
        GenericSpecificationsBuilder<UserEntity> specBuilder = new GenericSpecificationsBuilder<>();
        return specBuilder.build(parser.parse(searchParameters), GenericSpecification::new);
    }
}
