package org.iteph.tipr.model;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "control_key", schema = "public")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class ControlKeyEntity extends MessageKeyEntity {
}
