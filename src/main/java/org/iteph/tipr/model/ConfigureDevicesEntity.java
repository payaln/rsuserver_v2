package org.iteph.tipr.model;

import javax.persistence.*;

@Entity
@Table(name = "configure_devices", schema = "public")
public class ConfigureDevicesEntity {
    private Integer id;
    private Integer id_sector;
    private Integer subsector;
    private String keys;
    private String tasks;
    private Integer view_type_id;
    private Integer status_id;
    private String state;
    private Boolean is_arch;
    private String dev_name;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_sector")
    public Integer getId_sector() {
        return id_sector;
    }

    public void setId_sector(Integer id_sector) {
        this.id_sector = id_sector;
    }

    @Basic
    @Column(name = "subsector")
    public Integer getSubsector() {
        return subsector;
    }

    public void setSubsector(Integer subsector) {
        this.subsector = subsector;
    }

    @Basic
    @Column(name = "keys")
    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    @Basic
    @Column(name = "tasks")
    public String getTasks() {
        return tasks;
    }

    public void setTasks(String tasks) {
        this.tasks = tasks;
    }

    @Basic
    @Column(name = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "view_type_id")
    public Integer getView_type_id() {
        return view_type_id;
    }

    public void setView_type_id(Integer view_type_id) {
        this.view_type_id = view_type_id;
    }

    @Basic
    @Column(name = "status_id")
    public Integer getStatus_id() {
        return status_id;
    }

    public void setStatus_id(Integer status_id) {
        this.status_id = status_id;
    }

    @Basic
    @Column(name = "is_arch")
    public Boolean getIs_arch() {
        return is_arch;
    }

    public void setIs_arch(Boolean is_arch) {
        this.is_arch = is_arch;
    }

    @Basic
    @Column(name = "dev_name")
    public String getDev_name() {
        return dev_name;
    }

    public void setDev_name(String dev_name) {
        this.dev_name = dev_name;
    }
}
