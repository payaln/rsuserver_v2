package org.iteph.tipr.model;

import javax.persistence.*;

@Entity
@Table(name = "message_key", schema = "public")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class MessageKeyEntity {
    private Integer id;
    private Integer idDevice;
    private String key;

    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "id_device")
    public Integer getIdDevice() {
        return idDevice;
    }

    public void setIdDevice(Integer idDevice) {
        this.idDevice = idDevice;
    }

    @Column(name = "key")
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
