package org.iteph.tipr.specification;

import com.google.common.base.Joiner;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CriteriaParser {

    private static Map<String, Operator> ops;

    private static String reg = "(\\w+?)(" + Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET) + ")(\\p{Punct}?)(\\{.+?}|\\w+)(\\p{Punct}?)";

    private static Pattern SpecCriteraRegex = Pattern.compile(reg, Pattern.UNICODE_CHARACTER_CLASS);


    private enum Operator {
        OR(1), AND(2);
        final int precedence;

        Operator(int p) {
            precedence = p;
        }
    }

    static {
        Map<String, Operator> tempMap = new HashMap<>();
        tempMap.put("AND", Operator.AND);
        tempMap.put("OR", Operator.OR);
        tempMap.put("or", Operator.OR);
        tempMap.put("and", Operator.AND);

        ops = Collections.unmodifiableMap(tempMap);
    }

    private static boolean isHigerPrecedenceOperator(String currOp, String prevOp) {
        return (ops.containsKey(prevOp) && ops.get(prevOp).precedence >= ops.get(currOp).precedence);
    }

    public Deque<?> parse(String searchParam) {

        Deque<Object> output = new LinkedList<>();
        Deque<String> stack = new LinkedList<>();
        List<String> lexems = new ArrayList<>();
        Matcher m = SpecCriteraRegex.matcher(searchParam);
        searchParam += " ";
        for (String lexem : searchParam.split(reg)) {
            String word = "";
            if(m.find()){
                word = m.group();
                int indexOper = searchParam.indexOf(lexem);
                int indexWord = searchParam.indexOf(word);
                if(indexOper > indexWord){
                    lexems.add(word.replace("{", "").replace("}", ""));
                    word = "";
                    if(m.find())
                        word = m.group();
                }
            }

            for(String op : lexem.split(" "))
                if (!op.isEmpty())
                    lexems.add(op);
            if(!word.isEmpty())
                lexems.add(word.replace("{", "").replace("}", ""));
        }

        for (String token : lexems) {
            if (ops.containsKey(token)) {
                while (!stack.isEmpty() && isHigerPrecedenceOperator(token, stack.peek())) {
                    output.push(stack.pop()
                            .equalsIgnoreCase(SearchOperation.OR_OPERATOR) ? SearchOperation.OR_OPERATOR : SearchOperation.AND_OPERATOR);
                }
                stack.push(token.equalsIgnoreCase(SearchOperation.OR_OPERATOR) ? SearchOperation.OR_OPERATOR : SearchOperation.AND_OPERATOR);
            } else if (token.equals(SearchOperation.LEFT_PARANTHESIS)) {
                stack.push(SearchOperation.LEFT_PARANTHESIS);
            } else if (token.equals(SearchOperation.RIGHT_PARANTHESIS)) {
                while (!stack.peek().equals(SearchOperation.LEFT_PARANTHESIS)) {
                    output.push(stack.pop());
                }
                stack.pop();
            } else {

                Matcher matcher = SpecCriteraRegex.matcher(token);
                while (matcher.find()) {
                    output.push(new SpecSearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5)));
                }
            }
        }
        while (!stack.isEmpty()) {
            output.push(stack.pop());
        }

        return output;
    }
}