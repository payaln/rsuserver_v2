import socket
import psycopg2
import redis

conn_string = "host='localhost' dbname='tipr' user='postgres' password='p0stgres'"

l_addr = '192.168.2.167'
port = 50001

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((l_addr, port))

conn = psycopg2.connect(conn_string)
cursor = conn.cursor()
cursor.execute("SELECT addr, port, is_udp, dev_name, keys FROM public.configure_devices")
rows = cursor.fetchall()

r = redis.StrictRedis(host='localhost', port=6379, db=0);

names = {}
for row in rows:
    if row[2]:
        names[row[0]] = row[3] + ":" + row[4]
        print row[3]
        sock.sendto("hello!", (row[0], row[1]))
        
try:
    while True:
        data, addr = sock.recvfrom(1024)
        print names[addr[0]], data
        r.set(names[addr[0]], data);

finally:
    sock.close()
