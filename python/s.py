import serial
import redis
import psycopg2

id = 3;

conn_string = "host='localhost' dbname='tipr' user='postgres' password='p0stgres'";
conn = psycopg2.connect(conn_string);
cursor = conn.cursor();
cursor.execute("SELECT id, dev_name, is_serial, baundrate, com FROM public.configure_devices WHERE id = %s", [id]);
device = cursor.fetchall();
for dev in device:
    device = dev;
    
cursor.execute("SELECT key FROM public.message_key WHERE id_device = %s", [device[0]]);
mkey = cursor.fetchall();
for mk in mkey:
    mkey = mk[0];

cursor.execute("SELECT key FROM public.control_key WHERE id_device = %s", [device[0]]);
ckey = cursor.fetchall();
for ck in ckey:
    ckey = ck[0];


if device[2]:
    ser = serial.Serial();
    ser.baudrate = device[3];
    ser.port = device[4];
    try:
        ser.open();
        r = redis.StrictRedis(host='localhost', port=6379, db=0);

        last_ckey = r.get(ckey);
        while 1:
            try:
                while ser.in_waiting:
                    r.set(device[1] + ':' + mkey, ser.readline().replace('\r\n',''));
                if last_ckey != r.get(ckey):
                    last_ckey = r.get(ckey);
                    ser.write(last_ckey);
            except:
                print "no connection";
                break;
    except:
        print "no connection";
            

            
